<?php 
class Post_model extends CI_Model{
    public function __construct(){
        $this->load->database();
    }
    public function add($data){

        $this->load->database();

        $count = $this->db->insert('posts',$data);
        if($count>0){
            return true;
        }else{
            return false;
        }

    }
    public function get_posts($slug = FALSE){
        if ($slug === FALSE){
            $query = $this->db->get('posts');
            return $query->result_array();
        }

        $query = $this->db->get_where('posts', array('slug' => $slug));
        return $query->row_array();
    }
}
?>