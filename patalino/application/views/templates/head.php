<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Patalino Website Sign up</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<?php echo base_url();?> css/style.css ">
        <link rel="stylesheet" href="https://bootswatch.com/4/sketchy/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?> css/normalize.css">
        <link rel="stylesheet" href="<?php echo base_url();?> css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo base_url();?> css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?> css/web-layout.css">
        <style type="text/css">
                       
            .logo{
                height: 5em;
                width: 5em;
                float: left;
                margin-left: 10px;
            }
            .logoname{
                height: 5em;
                width: 47em;
                float: center;
                margin-left: 40px;
            }
            .orb {          
                display: inline-block;              
                transition: all 2s ease-out;
            }
            .orb:hover {
                transform: scale(1.5);
                opacity: 1;
                cursor: pointer;
                animation-play-state: paused;
            }
            .margins{
                margin: 30px;
            }
             .phone-info{
                margin-top: -30px;
            }
            .bg-color{
                background: rgba(226,226,226,1);
                background: -moz-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(226,226,226,1) 7%, rgba(209,209,209,1) 43%, rgba(219,219,219,1) 61%, rgba(254,254,254,1) 96%, rgba(254,254,254,1) 100%);
                background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(226,226,226,1)), color-stop(7%, rgba(226,226,226,1)), color-stop(43%, rgba(209,209,209,1)), color-stop(61%, rgba(219,219,219,1)), color-stop(96%, rgba(254,254,254,1)), color-stop(100%, rgba(254,254,254,1)));
                background: -webkit-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(226,226,226,1) 7%, rgba(209,209,209,1) 43%, rgba(219,219,219,1) 61%, rgba(254,254,254,1) 96%, rgba(254,254,254,1) 100%);
                background: -o-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(226,226,226,1) 7%, rgba(209,209,209,1) 43%, rgba(219,219,219,1) 61%, rgba(254,254,254,1) 96%, rgba(254,254,254,1) 100%);
                background: -ms-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(226,226,226,1) 7%, rgba(209,209,209,1) 43%, rgba(219,219,219,1) 61%, rgba(254,254,254,1) 96%, rgba(254,254,254,1) 100%);
                background: linear-gradient(to bottom, rgba(226,226,226,1) 0%, rgba(226,226,226,1) 7%, rgba(209,209,209,1) 43%, rgba(219,219,219,1) 61%, rgba(254,254,254,1) 96%, rgba(254,254,254,1) 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#fefefe', GradientType=0 );
            }
            @media only screen and (max-width: 600px){
                .logo{
                    height: 5em;
                    width: 5em;
                    float: left;
                    margin-left: 5px;
                }
                .logoname{
                    height: 5em;
                    width:  20em;
                    float: center;
                    margin-left: 45px;
                    
                }
                 #page-content{
                    margin-top: -45px;
                 }
            }
        </style>
        
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        
    </head>
    <body class="bg-color">
       
        
        <div class="site-bg"></div>
        <div class="site-bg-overlay"></div>
        <!-- TOP HEADER -->
        <div class="top-header" style="height: 7em">
            <!--<img src="images/logo.png" alt="" class="logo"  >-->
            <div class="container">
                <img src="images/patalino.png" alt="" class="logoname">
            </div>
                
        </div> <!-- .top-header -->