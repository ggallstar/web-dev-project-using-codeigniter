<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


<!DOCTYPE html>
<html class="no-js">
     <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Patalino Website</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="<?php echo base_url();?> css/style.css ">
        <link rel="stylesheet" href="https://bootswatch.com/4/sketchy/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?> css/normalize.css">
        <link rel="stylesheet" href="<?php echo base_url();?> css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo base_url();?> css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?> css/web-layout.css">
        <style type="text/css">
            #page-content{
                margin-top: -50px;
                 font-size: 20px;
            }
            .h1{
                font-size: 60px;
                font-family: 'Gloria Hallelujah', cursive;
                text-shadow: 2px 4px #525151;
            }           
            .logo{
                height: 5em;
                width: 5em;
                float: center;
                margin-left: 10px;
                margin-top: -15px;
            }
            .logoname{
                height: 5em;
                width: 50em;
                float: center;
               
            }
            .orb {          
                display: inline-block;              
                transition: all 2s ease-out;
            }
            .orb:hover {
                transform: scale(1.5);
                opacity: 1;
                cursor: pointer;
                animation-play-state: paused;
            }
            .margins{
                margin: 30px;
            }
            .phone-info{
                margin-top: -30px;
            }
            .bg-color{
                background: rgba(226,226,226,1);
                background: -moz-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(226,226,226,1) 7%, rgba(209,209,209,1) 43%, rgba(219,219,219,1) 61%, rgba(254,254,254,1) 96%, rgba(254,254,254,1) 100%);
                background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(226,226,226,1)), color-stop(7%, rgba(226,226,226,1)), color-stop(43%, rgba(209,209,209,1)), color-stop(61%, rgba(219,219,219,1)), color-stop(96%, rgba(254,254,254,1)), color-stop(100%, rgba(254,254,254,1)));
                background: -webkit-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(226,226,226,1) 7%, rgba(209,209,209,1) 43%, rgba(219,219,219,1) 61%, rgba(254,254,254,1) 96%, rgba(254,254,254,1) 100%);
                background: -o-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(226,226,226,1) 7%, rgba(209,209,209,1) 43%, rgba(219,219,219,1) 61%, rgba(254,254,254,1) 96%, rgba(254,254,254,1) 100%);
                background: -ms-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(226,226,226,1) 7%, rgba(209,209,209,1) 43%, rgba(219,219,219,1) 61%, rgba(254,254,254,1) 96%, rgba(254,254,254,1) 100%);
                background: linear-gradient(to bottom, rgba(226,226,226,1) 0%, rgba(226,226,226,1) 7%, rgba(209,209,209,1) 43%, rgba(219,219,219,1) 61%, rgba(254,254,254,1) 96%, rgba(254,254,254,1) 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#fefefe', GradientType=0 );
            }
            
            @media only screen and (max-width: 600px){
                .logo{
                   
                    display:none;
                }
                .logoname{
                    height: 5em;
                    width: 15em;
                    float: center;
                    margin-left: 15px;
                }
                #logout{
                    float: right;
                    margin: -30px 10px; 
                }
               .phone-info{
                    font-size: 20px;
                    float: left;
                    margin: 10px;
                }
                #menus{
                    float: right;
                }
                #page-content{
                    margin-top: 15px;
                }
            }
            
        </style>
    </head>
    <body class="bg-color">     
    <nav class="navbar navbar-inverese">
        <div class="container">
            
               
            
            <img src="images/logo.png" alt="" class="logo">                
            <img src="<?= base_url()?>images/patalino.png?>" alt="" class="logoname">
            <div id="navbar">
                <ul class="nav navbar-nav" id="menus">
                    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
                    <!--<li><a href="<?php echo base_url(); ?>help">Help</a></li>-->
                    <li><a href="<?php echo base_url(); ?>ranks">Ranks</a></li>
                    <li><a href="<?php echo base_url(); ?>about">About</a></li>
                    <li><a href="<?php echo site_url('user/logout');?>">Logout</a></li>
                </ul>
            </div>
        </div>
    </nav>

        
    