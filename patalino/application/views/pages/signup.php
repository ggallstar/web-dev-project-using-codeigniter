<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Patalino Website Sign up</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<?php echo base_url();?> css/style.css ">
        <link rel="stylesheet" href="https://bootswatch.com/4/sketchy/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?> css/normalize.css">
        <link rel="stylesheet" href="<?php echo base_url();?> css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo base_url();?> css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?> css/web-layout.css">
        <style type="text/css">
                       
            .logo{
                height: 5em;
                width: 5em;
                float: left;
                margin-left: 10px;
            }
            .logoname{
                height: 5em;
                width: 47em;
                float: center;
                margin-left: 40px;
                margin-top: 20px;
            }
            .orb {          
                display: inline-block;              
                transition: all 2s ease-out;
            }
            .orb:hover {
                transform: scale(1.5);
                opacity: 1;
                cursor: pointer;
                animation-play-state: paused;
            }
            .margins{
                margin: 30px;
            }
             .phone-info{
                margin-top: -30px;
            }
            .bg-color{
                background: rgba(226,226,226,1);
                background: -moz-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(226,226,226,1) 7%, rgba(209,209,209,1) 43%, rgba(219,219,219,1) 61%, rgba(254,254,254,1) 96%, rgba(254,254,254,1) 100%);
                background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(226,226,226,1)), color-stop(7%, rgba(226,226,226,1)), color-stop(43%, rgba(209,209,209,1)), color-stop(61%, rgba(219,219,219,1)), color-stop(96%, rgba(254,254,254,1)), color-stop(100%, rgba(254,254,254,1)));
                background: -webkit-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(226,226,226,1) 7%, rgba(209,209,209,1) 43%, rgba(219,219,219,1) 61%, rgba(254,254,254,1) 96%, rgba(254,254,254,1) 100%);
                background: -o-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(226,226,226,1) 7%, rgba(209,209,209,1) 43%, rgba(219,219,219,1) 61%, rgba(254,254,254,1) 96%, rgba(254,254,254,1) 100%);
                background: -ms-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(226,226,226,1) 7%, rgba(209,209,209,1) 43%, rgba(219,219,219,1) 61%, rgba(254,254,254,1) 96%, rgba(254,254,254,1) 100%);
                background: linear-gradient(to bottom, rgba(226,226,226,1) 0%, rgba(226,226,226,1) 7%, rgba(209,209,209,1) 43%, rgba(219,219,219,1) 61%, rgba(254,254,254,1) 96%, rgba(254,254,254,1) 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#fefefe', GradientType=0 );
            }
            @media only screen and (max-width: 600px){
                .logo{
                    height: 5em;
                    width: 5em;
                    float: left;
                    margin-left: 5px;
                }
                .logoname{
                    height: 5em;
                    width:  20em;
                    float: center;
                    margin-left: 45px;
                    
                }
                 #page-content{
                    margin-top: -45px;
                 }
            }
        </style>
        
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        
    </head>
    <body class="bg-color">
       
        
        <div class="site-bg"></div>
        <div class="site-bg-overlay"></div>
        <!-- TOP HEADER -->
        <div class="top-header" style="height: 7em; margin-bottom: -30px">
            <!--<img src="images/logo.png" alt="" class="logo"  >-->
            <div class="container">
                <img src="<?= base_url()?>images/patalino.png?>" alt="" class="logoname">
            </div>
                
        </div> <!-- .top-header -->

        <div class="container" id="page-content">

            <div class="row margins" >
                <div class="col-md-9 col-sm-12 content-holder">
                    <!-- CONTENT -->
                    <div id="menu-container">                                   
                        <div class="row">
                            <?php if (validation_errors()) : ?>
                            <div class="col-md-12">
                                <div class="alert alert-danger" role="alert">
                                    <?= validation_errors() ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (isset($error)) : ?>
                            <div class="col-md-12">
                                <div class="alert alert-danger" role="alert">
                                    <?= $error ?>
                                </div>
                            </div>
                        <?php endif; ?>
                            <div class="col-md-8 col-sm-8">
                                <div class="box-content">
                                    <h3 class="widget-title">Sign up</h3>
                                    <p>Please fill this form to create an account.</p>
                                        <!--<?= form_open() ?>-->
                                        <form action="<?php echo site_url('User/register'); ?>" method="post">
                                            <div class="form-group ">
                                                <label for="username">Username</label>
                                                <input type="text" class="form-control" id="username" name="username" placeholder="Enter a username">
                                                <p class="help-block">At least 6 characters, letters or numbers only</p>
                                            </div>    
                                            <div class="form-group ">
                                                <label for="password">Password</label>
                                                <input type="password" class="form-control" id="password" name="password" placeholder="Enter a password">
                                                <p class="help-block">At least to 6 characters</p>
                                            </div>
                                            <div class="form-group ">
                                                <label for="password_confirm">Confirm password</label>
                                                <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirm your password">
                                                <p class="help-block">Must match your password</p>
                                            </div>
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-primary" value="Submit">
                                                <input type="reset" class="btn btn-default" value="Reset">
                                            </div>
                                            <p>Already have an account? <a href="<?php echo site_url(); ?>User/login">Login here</a>.</p>
                                        </form>                                         
                                </div>
                            </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="box-content">
                                        <h3 class="widget-title">Sign Up</h3>
                                        <p>Creating account is mandatory in order to play this game. Log in to play the game, thank you!</p>
                                    </div>
                                </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
