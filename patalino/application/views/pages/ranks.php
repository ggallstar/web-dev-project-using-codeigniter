<?php defined('BASEPATH') OR exit('No direct script access allowed');
   
    $username = $this->session->userdata('username');
   
 ?>        
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head><meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= $title ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    	<body onload = "generateQuestion()" class="bg-color">
		<div class="site-bg"></div>
        <div class="site-bg-overlay"></div>
		<!-- TOP HEADER -->
        <div class="top-header" style="height: 9em; margin-top:-35px;">
			
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <br><p class="phone-info" style="margin-top: -10px">Welcome <a><?php  echo $this->session->userdata('username');?></a> !</p>
						
                    </div>      
						
                </div>                
            </div>            
        </div> <!-- .top-header -->

        <div class="container" id="page-content">
           <div class="row" >                
                <div class="col-md-12 col-sm-12 content-holder">
                    <!-- CONTENT -->
                    <div id="menu-container">                       
						
			<div class='row'>
				<div class='col-md-4 col-sm-5'>
					<div class='box-content'>
						<h3 class='widget-title'>Score Ranks</h3>	
							<div class='project-item'>	
								<ul class='progress-bars'>
							<?php  
					         $ctr=0;
	$get_data=$this->User_model->get_table_data();
?>
<table class="table table-bordered">
	<thead>
		<tr>
			<th> Placement </th>
			<th> Username </th>
			<th> Score </th>
			
		</tr>
	</thead>
	<tbody>
	<?php foreach($get_data as $val){ $ctr++;?>
		<tr>
			<td><?php echo $ctr;?></td>
			<td><?php echo $val->username; ?></td>
			<td><?php echo $val->score; ?></td>
			
		</tr>
	<?php } ?>
	</tbody>
</table>
					
					       
												
												
											</ul>
                                        </div>
                                    </div>
                                </div>
                                
					</div>
                </div>
			</div>
		</div>
                