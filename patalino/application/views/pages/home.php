<?php defined('BASEPATH') OR exit('No direct script access allowed');
   
    $username = $this->session->userdata('username');
    
 ?>        
        
<div class="site-bg"></div>
        <div class="site-bg-overlay"></div>
        <!-- TOP HEADER -->
        <div class="top-header" style="height: 3em; margin-top:-15px;">
            
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <br><p class="phone-info" >Welcome <a><?php  echo $this->session->userdata('username');?></a> !</p>
                        
                    </div>      
                        
                        
 
                </div>                
            </div>            
        </div> <!-- .top-header -->

        <div class="container" id="page-content">
                    
            <div class="row margins" >
                <div class="col-md-12 col-sm-12 content-holder">
                    <!-- CONTENT -->
                       
                        <div id="menu-1" class="homepage home-section text-center">
                            <div class="welcome-text">
                                          
                                <h2><strong><marquee id="myMarquee" scrollamount="10" loop="-1" >"Prove them wrong that you are right?"</marquee></strong></h2>
                                <p>This is a Quick Maths Quiz website.</p><br/>
                                <p><strong>INTRODUCTIONS:</strong>
                                <ul>
                                    <li>Answer as many as you can before the time runs out.</li>
                                    <li>Every question has 5 seconds for you to answer it correctly.</li>
                                    <li>Every incorrect answer will deduct 10 points from you total points.</li>
                                    <li>Once your 5-second time is over, it will deduct 10 points multiplied by the time consumed  from your total points.</li>
                                    <li>Every correct answer will merit you 10 points multiplied by the remaining time left from your 5-second per question.</li>
                                    
                                </ul>   
                                <h1  class="h1 orb" ><strong><a href="<?php echo base_url(); ?>gameplay">Play</a></strong></h1>
                                
                            </div>
                        </div>              
                               
                </div>
            </div>
        </div>
