<style type="text/css">
			 #page-content{
                margin-top: -50px;
                 font-size: 20px;
            }
            .h1{
                font-size: 60px;
                font-family: 'Gloria Hallelujah', cursive;
                text-shadow: 2px 4px #525151;
            }           
            .logo{
                height: 5em;
                width: 5em;
                float: center;
                margin-left: 10px;
                margin-top: -15px;
            }
            .logoname{
                height: 5em;
                width: 50em;
                float: center;
               
            }margin-left: -200px;
			}
			.orb {			
				display: inline-block;				
				transition: all 2s ease-out;
			}
			.orb:hover {
				transform: scale(1.5);
				opacity: 1;
				cursor: pointer;
				animation-play-state: paused;
			}
			#logout{
					float: right;
					margin-top: 15px;
			}
			#statetext{
				font-size: 30px;
				margin-top: 50px;
               
			}
			#question{				
				font-family: 'Nanum Pen Script', cursive;
				font-size: 40px;
			}
			#playeranswer{
				width: 70%;
                height: 20vh;
                font-size: 100px;
                text-align: center;
                margin: 10px; 
                border: solid;
                border-width: 1px;
                border-radius: 20px;
				color: black;
			}
			#imgstatus{
					height: 30vh;
					weight: 30vw;
					float: right;
			}
			.margins{
                margin: 30px;
            }
            .phone-info{
                margin-top: -50px;
            }
			.boton {
		    	background-color: #A4978E;
			    color: white;
			    padding: 10px 20px;
			    border: none;
			    cursor: pointer;
		    	width: 30%;
			    opacity: 0.8;
			    height: 100%;
                font-size: 30px;
                border-width: 1px;
                border-radius: 20px;
                margin-left: 45spx;
			}
            .bg-color{
                background: rgba(226,226,226,1);
                background: -moz-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(226,226,226,1) 7%, rgba(209,209,209,1) 43%, rgba(219,219,219,1) 61%, rgba(254,254,254,1) 96%, rgba(254,254,254,1) 100%);
                background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(226,226,226,1)), color-stop(7%, rgba(226,226,226,1)), color-stop(43%, rgba(209,209,209,1)), color-stop(61%, rgba(219,219,219,1)), color-stop(96%, rgba(254,254,254,1)), color-stop(100%, rgba(254,254,254,1)));
                background: -webkit-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(226,226,226,1) 7%, rgba(209,209,209,1) 43%, rgba(219,219,219,1) 61%, rgba(254,254,254,1) 96%, rgba(254,254,254,1) 100%);
                background: -o-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(226,226,226,1) 7%, rgba(209,209,209,1) 43%, rgba(219,219,219,1) 61%, rgba(254,254,254,1) 96%, rgba(254,254,254,1) 100%);
                background: -ms-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(226,226,226,1) 7%, rgba(209,209,209,1) 43%, rgba(219,219,219,1) 61%, rgba(254,254,254,1) 96%, rgba(254,254,254,1) 100%);
                background: linear-gradient(to bottom, rgba(226,226,226,1) 0%, rgba(226,226,226,1) 7%, rgba(209,209,209,1) 43%, rgba(219,219,219,1) 61%, rgba(254,254,254,1) 96%, rgba(254,254,254,1) 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#fefefe', GradientType=0 );
            }
			/* hide HTML5 Up and Down arrows. */
			input[type="number"]::-webkit-outer-spin-button, input[type="number"]::-webkit-inner-spin-button {
				-webkit-appearance: none;
				margin: 0;
			}
			input[type="number"]{
				-moz-appearance: textfield;
			}
			@media only screen and (max-width: 600px){
				.logoname{
                    height: 5em;
                    width: 15em;
                    float: center;
                    margin-left: 15px;
                }
                #logout{
                    float: right;
                    margin: -30px 10px; 
                }
                .phone-info{
                    font-size: 20px;
                    float: left;
                    margin-top: 10px;
                }
                .margin{
                	margin: 30px;
                }
                #menus{
                    float: right;
                }
                #game{
                	margin: 10px;
                }
				h1{
					font-size: 15px;
					color: white;
				}
				.h1{
					font-size: 60px;
					font-family: 'Gloria Hallelujah', cursive;
					text-shadow: 2px 4px #525151;
				}
				.orb {			
					display: inline-block;				
					transition: all 2s ease-out;
				}
				.orb:hover {
					transform: scale(1.5);
					opacity: 1;
					cursor: pointer;
					animation-play-state: paused;
				}
				#statetext{
					font-size: 30px;
					margin-top: 50px;
				   
				}
				#question{				
					font-family: 'Nanum Pen Script', cursive;
					font-size: 30px;
					
				}
				#playeranswer{
					width: 100%;
					height: 20vh;
					font-size: 100px;
					text-align: center;
					border: solid;
					border-width: 1px;
					border-radius: 20px;
					color: black;
				}
				#imgstatus{
						height: 30vh;
						width: 30vw;
						float: right;
				}
				
				.boton {
					background-color: #A4978E;
					color: white;
					padding: 10px 20px;
					border: none;
					cursor: pointer;
					width: 30%;
					opacity: 0.8;
					height: 100%;
					font-size: 10px;
					border-width: 1px;
					border-radius: 10px;
					margin-left: 50px;
					margin-top: 10px;
				}
                /* hide HTML5 Up and Down arrows. */
				input[type="number"]::-webkit-outer-spin-button, input[type="number"]::-webkit-inner-spin-button {
					-webkit-appearance: none;
					margin: 0;
				}
				input[type="number"]{
					-moz-appearance: textfield;
				}
			}
			
		</style>
		<script type="text/javascript" src="js/patalino.js"></script>
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
		<script>
			//query scripts
		function saveScore(){
			$.post("gameplay.php,
			{
			score: $('#scoreText).val()
			},
			function(data, status){
				document.getElementById("#scoreText").innerHTML = data;
			}");
		}
		</script>
    </head>
    <body onload = "generateQuestion()" class="bg-color">
		<div class="site-bg"></div>
        <div class="site-bg-overlay"></div>
		<!-- TOP HEADER -->
        <div class="top-header" style="height: 7em">
			
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <br><p class="phone-info" >Welcome <a><?php  echo $this->session->userdata('username');?></a> !</p>
						
                    </div>      
						
 
                </div>                
            </div>            
        </div> <!-- .top-header -->

       

        <div class="container" id="page-content">
           <div class="row" >                
                <div class="col-md-12 col-sm-12 content-holder">
                    <!-- CONTENT -->
                    <div id="menu-container">                       
                       <div class="col-md-3 hidden-sm">
                    
							
						</div>
                        <div class="row rounded" id="game">				
                    
<?php
	include 'config.php';
	//$sql = "SELECT *  FROM users where username=''";
	//$link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
	
	/*$mysqli = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
	$new_score = mysqli_escape_string($link,$_POST['scoreText']);
	$q = mysqli_escape_string($link, $new_score);
	$query = "INSERT INTO users (score) VALUES ('$new_score')";*/
 
	
?>

							<h1 id ="globaltimer"></h1>
							<h1 id ="scoreText">Score: 0</h1>
							<h1 id ="timer"></h1><br>
							  
							<h1 id ="question"></h1> <br />
							
						
							<input type="number" id="playeranswer" onkeydown = "if (event.keyCode == 13) document.getElementById('submit').click()">
							<!--<img src = "images/logo.png" id="imgstatus" ><br/>	-->
							<input type="button" class="boton" id="submit" value="Submit Answer" onclick="checkAnswer()">
							<input type="button" class="boton" id="reset" value="Reset Game" onclick="resetGame()" disabled = true>
							
								<br />
								
						</div>
							<b><p id ="statetext" style="text-align: center"></p></b>
                    </div>
				</div>
			</div>
		</div>