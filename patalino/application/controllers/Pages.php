<?php
class Pages extends CI_Controller {
    public function view($page = 'home'){
        //check if a particular .php file exists within views > pages
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php'))
            show_404();
        
        $data['title'] = ucfirst($page); // represents the variable we want to pass in the view

        /*if($this->load->view('pages/login')){
            
            $this->load->view('templates/footer');
        }else{*/
        //load the header, the page, and the footer
        $this->load->view('templates/header');
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer');
        //}
    }
}