<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 * 
 * @extends CI_Controller
 */
class User extends CI_Controller {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->model('user_model');
		
	}
	
	
	public function index() {
		if(!isset($is_logged_in) || $is_logged_in != true)
    {
        echo 'You don\'t have permission to access this page.';
       	redirect('login'); 
       	// header("location: login");
        // die("Redirecting to login.php");
        
    }else if($this->load->view('pages/home') && isset($is_logged_in)){
    	redirect('login', 'refresh');
    }else{
    return true;

    }
		// 	redirect('login','refresh');
		// }else{
		// 	// $this->load->view('templates/header');
		// 	// $this->load->view('pages/home');
		// 	// $this->load->view('templates/footer');
		// 	redirect('home','refresh');
		// }
	}
	
	/**
	 * register function.
	 * 
	 * @access public
	 * @return void
	 */
	public function register() {
		
		// create the data object
		$data = new stdClass();
		
		// load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		// set validation rules
		$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric|min_length[4]|is_unique[users.username]', array('is_unique' => 'This username already exists. Please choose another one.'));
		//$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('password_confirm', 'Confirm Password', 'trim|required|min_length[6]|matches[password]');
		
		if ($this->form_validation->run() === false) {
			
			// validation not ok, send validation errors to the view
			//$this->load->view('templates/head');
			$this->load->view('pages/signup', $data);
			$this->load->view('templates/footer');
			
		} else {
			
			// set variables from the form
			$username = $this->input->post('username');
			//$email    = $this->input->post('email');
			$password = $this->input->post('password');
			
			if ($this->user_model->create_user($username, $password)) {
				
				// user creation ok
				// $this->load->view('templates/header');
				// $this->load->view('pages/home', $data);
				// //redirect('home',$data);
				// $this->load->view('templates/footer');
				redirect('login');
			} else {
				
				// user creation failed, this should never happen
				$data->error = 'There was a problem creating your new account. Please try again.';
				
				// send error to the view
				//$this->load->view('templates/head');
				// $this->load->view('pages/signup', $data);
				// $this->load->view('templates/footer');
				redirect('login');
			}
			
		}
		
	}
		
	/**
	 * login function.
	 * 
	 * @access public
	 * @return void
	 */
	public function login() {
		
		// create the data object
		$data = new stdClass();
		
		// load form helper and validation library
		$this->load->helper('form','url');
		$this->load->library('form_validation');
		
		// set validation rules
		$this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		if ($this->form_validation->run() == false) {
			
			// validation not ok, send validation errors to the view
			 //$this->load->view('templates/head');
			 $this->load->view('pages/login');
			 $this->load->view('templates/footer');
			//redirect('login');
		} else {
			
			// set variables from the form
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			
			if ($this->user_model->resolve_user_login($username, $password)) {
				
				$user_id = $this->user_model->get_user_id_from_username($username);
				$user    = $this->user_model->get_user($user_id);
				
				// set session user datas
				$_SESSION['user_id']      = (int)$user->id;
				$_SESSION['username']     = (string)$user->username;
				$_SESSION['logged_in']    = (bool)true;
				$_SESSION['is_confirmed'] = (bool)$user->is_confirmed;
				$_SESSION['is_admin']     = (bool)$user->is_admin;
				
				// user login ok
				// $this->load->view('templates/header');
				// //$this->load->view('pages/home', $data);
				// redirect('home');
				// $this->load->view('templates/footer');
				redirect('home');
			} else {
				
				// login failed
				$data->error = 'Wrong username or password.';
				
				// send error to the view
				//$this->load->view('templates/head');
				$this->load->view('pages/login', $data);
				$this->load->view('templates/footer');
				
			}
			
		}
		
	}
	
	/**
	 * logout function.
	 * 
	 * @access public
	 * @return void
	 */
	public function logout() {
		
		// // create the data object
		$data = new stdClass();
		$this->load->helper('url');
		if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {
			
			// remove session datas
			foreach ($_SESSION as $key => $value) {
				unset($_SESSION[$key]);
			}
			
			// user logout ok
			//session_destroy();
			$this->session->sess_destroy();
			$this->index();
			//redirect('login','refresh');
			//$this->load->view('templates/header');
			// $this->load->view('pages/login', $data);
			// $this->load->view('templates/footer');
			
		} else {
			
			// there user was not logged in, we cannot logged him out,
			// redirect him to site root
			//header("location: login");
			$this->session->sess_destroy();
			$this->index();
        	die("Redirecting to login.php");
			
		}
		// session_destroy();	
		// $this->index();
	}
	public function get_score()  
      {  
         //load the database  
         $this->load->database();  
					         //load the model  
					         $this->load->model('user_model');  
					         //load the method of model  
					         $data['data']=$this->User->get_score();  
					         //return the data in view  
					         $this->load->view('pages/ranks', $data);  
      }  
}
