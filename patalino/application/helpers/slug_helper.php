<?php

if (!function_exists('create_slug')) {
function create_slug($string)
{
    $slug = trim($string);
    $slug = strtolower($slug);
    $slug = str_replace(' ', '-', $slug);

    return $slug;
} } ?>